﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace CSharpPractModule16
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                string m = "";
                Console.WriteLine("Enter 1 to start task 1");
                Console.WriteLine("Enter 2 to start task 2");

                m = Console.ReadLine();

                if (m == "1")
                {
                    char[] chArray;
                    string inputPath = "input.bin";
                    using (FileStream fs = File.OpenRead(inputPath))
                    {
                        byte[] buffer = new byte[fs.Length];
                        fs.Read(buffer, 0, buffer.Length);
                        chArray = System.Text.Encoding.UTF8.GetChars(buffer);
                    }

                    int[] counters = new int[256];
                    char[] letters = new char[256];

                    for (int i = 0; i < 256; i++)
                    {
                        counters[i] = 0;
                    }

                    for (int i = 0; i < chArray.Length; i++)
                    {
                        int j = 0; ;
                        while (j < 256)
                        {
                            if (chArray[i] == j)
                            {
                                counters[j]++;
                                break;
                            }
                            j++;
                        }
                    }

                    for (int i = 0; i < letters.Length; i++)
                    {
                        Console.WriteLine((letters[i] = (char)i) + "\t" + counters[i] + "\t");
                    }
                }
                Console.WriteLine();
                Console.ReadLine();

                if (m == "2")
                {
                    string outputPath = "output.txt";
                    using (StreamWriter sw = new StreamWriter(outputPath, false, System.Text.Encoding.Default))
                    {
                        sw.WriteLine("Ruslan");
                        sw.WriteLine("Tyo");
                        sw.WriteLine("29");
                        Console.ReadLine();
                    }
                }
            }
        }
    }
}
